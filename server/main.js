import {Meteor} from 'meteor/meteor';
import {Session} from 'meteor/session';
import {HTTP} from 'meteor/http';

Meteor.startup(() => {
});

if (Meteor.isServer) {
    Meteor.methods({
        resturantData: function () {
            this.unblock();
            return HTTP.call("GET", "http://77.68.80.27:4010/marketplaceapi/landingrestaurantlist?&lat=51.7354&long=-0.315091&pageindex=1&pagesize=30");
        }
    });
}
