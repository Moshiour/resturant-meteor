import {Template} from 'meteor/templating';
import {HTTP} from 'meteor/http';

import './main.html';


Template.resturantview.created = function () {
    Meteor.call("resturantData", function (error, results) {
        Session.set('resturantData', results.data.data.takingorders.listdata);
    });
};

Template.resturantview.helpers({
    resturantData: function () {
        return Session.get('resturantData');
    }
});

